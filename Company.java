//Superclass
public class Company {

    //encapsulations
    public String name;
    public int numOfEmp;

    public Company() {
        this.name=name;
        this.numOfEmp=numOfEmp;
    }

    //get and set Company name
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //get and set Number Of Employees which are working in this company
    public int getNumOfEmp() {
        return numOfEmp;
    }

    public void setNumOfEmp(int numOfEmp) {
        this.numOfEmp = numOfEmp;
    }

    //polymorphism
    public void position(){
        System.out.println(getName() + " is a big IT company!");
    }

    //shows in short all information about this Company
    public String toString(){
        return "Company name " + getName() + ", number of employees: " + getNumOfEmp() + ";";
    }

}

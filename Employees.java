//Subclass
public class Employees extends Company{

    //encapsulations
    private int empId;
    private String empName;
    private int empSalary;


    public Employees(){
        super();

        this.empId = empId;
        this.empName = empName;
        this.empSalary = empSalary;

    }

    //get and set Employee id
    public int getEmpId() {
        return empId;
    }
    public void setEmpId(int empId) {
        this.empId = empId;
    }

    //get and set Employee name
    public String getEmpName() {
        return empName;
    }
    public void setEmpName(String empName) {
        this.empName = empName;
    }

    //get and set Employee salary
    public int getEmpSalary() {
        return empSalary;
    }
    public void setEmpSalary(int empSalary) {
        this.empSalary = empSalary;
    }

    //polymorphism
    public void position(){
        System.out.println(getEmpName() + " is a hardworking employee!");
    }

    //shows in short all information about this Employee
    public String toString(){
        return "Employee id: " + getEmpId() + ", name: " + getEmpName() + ", salary: " + getEmpSalary() + ";";
    }



}

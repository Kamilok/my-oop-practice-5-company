public class Test {

    public static void main(String[] args){
        //I crete new Company
        Company comp = new Company();
        //I set company name
        comp.setName("Google");
        //I set number of employees that are working in this company
        comp.setNumOfEmp(1000);
        //Shows all information about this company
        System.out.println(comp);
        //Shows company position
        comp.position();


        //I create new Employee
        Employees emp = new Employees();
        //I set Employee id
        emp.setEmpId(1);
        //I set Employee name
        emp.setEmpName("Alex");
        //I set Employee salary
        emp.setEmpSalary(50000);
        //Shows all information about this Employee
        System.out.println(emp);
        //Shows Employee position
        emp.position();

    }

}

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Driver {

    //SQL connection link
    private String jdbcURL = "jdbc:mysql://localhost:3306/employees";
    private String username = "root";
    private String password = "root";

    //SQL formula that shows Employee salary that is more than or equal to 50000
    private static final String SELECT_EMPLOYEE_SALARY_SQL = "select * from employees where salary>=50000";

    //connection method
    protected Connection getConnection(){
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, username, password);
        }

        catch (SQLException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    //shows Employee salary that is more than or equal to 50000 using String that I created
    public void salaryEmployee(Employees employee) throws SQLException {
        try (
                Connection connection = getConnection();
                PreparedStatement pstmt = connection.prepareStatement(SELECT_EMPLOYEE_SALARY_SQL);) {
                //after selecting salary using this I update information in the employee table in SQL
                pstmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
